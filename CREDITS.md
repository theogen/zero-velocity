# Credits

[jam]: https://itch.io/jam/weekly-game-jam-144
[love]: https://love2d.org
[font]: https://fonts.google.com/specimen/Press+Start+2P
[music]: https://ericskiff.com/music/
[laser]: https://freesound.org/people/sharesynth/sounds/341666/

This game has originally been developed by Theogen Ratkin for the
[Weekly Game Jam #144][jam].

Engine: [LÖVE framework][love]

Font: [Press Start 2P by CodeMan32][font]

Music: [Underclocked by Eric Skiff from the album Resistor Anthems][music]

Laser sound: [Electricity00 by sharesynth][laser]
